import { Component, OnInit, EventEmitter, Output, forwardRef, Inject } from '@angular/core';
import { DestinoViaje } from '../../models/destino-viaje.model';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { fromEvent, pipe, } from 'rxjs';
import { map, filter, debounceTime, distinctUntilChanged, switchMap } from 'rxjs/operators';
import { ajax } from 'rxjs/ajax';
import { APP_CONFIG, AppConfig } from '../../app.module';

@Component({
    selector: 'app-form-destino-viaje',
    templateUrl: './form-destino-viaje.component.html',
    styleUrls: ['./form-destino-viaje.component.scss']
})
export class FormDestinoViajeComponent implements OnInit {
    @Output() onItemAdded: EventEmitter<DestinoViaje> = new EventEmitter();
    fg: FormGroup;
    minLongitud: number = 5;
    searchResults: string[] = [];

    constructor(fb: FormBuilder, @Inject(forwardRef(() => APP_CONFIG)) private config: AppConfig ) {
        this.fg = fb.group({
            nombre: ['', Validators.compose([
                Validators.required,
                this.nombreValidator,
                this.nombreValidatorParametrizable(this.minLongitud)
            ])],
            url: ['', Validators.required]
        });

        this.fg.valueChanges.subscribe((form: any) => {
            //console.log(form);
        });

     }

    ngOnInit(): void {
        let elemNombre = <HTMLInputElement>document.getElementById('nombre');
        const typeHead = fromEvent(elemNombre, 'input');

        typeHead.pipe(
            map((e: KeyboardEvent) =>  (e.target as HTMLInputElement).value),
            filter((text:string) => text.length > 3),
            debounceTime(200),
            distinctUntilChanged(),
            switchMap((text: string) => ajax(`${this.config.apiEndpoint}/ciudades/?q=${text}`))
        ).subscribe(ajaxResponse => {
            this.searchResults = ajaxResponse['response'];
        });
        
    }

    guardar(nombre: string, url: string) {
        const d = new DestinoViaje(nombre, url);
        //console.log(d);
        this.onItemAdded.emit(d);
    }

    nombreValidator(control: FormControl): { [s: string]: boolean } {
        const l = control.value.toString().trim().length;
        if(l < 5 && l > 0){
            return { invalidNombre: true }
        }
        return null;
    }

    nombreValidatorParametrizable(minLong: number){
        return (control: FormControl): { [s: string ]: boolean } | null  => {
            const l = control.value.toString().trim().length;
            if(l < minLong && l > 0){
                return { minLongNombre: true }
            }
            return null;
        }
    }

}
