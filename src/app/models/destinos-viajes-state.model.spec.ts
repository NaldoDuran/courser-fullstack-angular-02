import { DestinoViaje } from './destino-viaje.model';
import { DestinosViajesState, initializeDestinosViajesState, InitMyDataAction, reducerDestinosViajes, NuevoDestinoAction, ElegidoFavoritoAction, EliminarDestinoAction, VoteDownAction, VoteUpAction, VoteResetAction } from './destinos-viajes-state.model';


describe('reducerDestinosViajes', () => {
    it('should reduce init data', () => {
        const prevState: DestinosViajesState = initializeDestinosViajesState();
        const action: InitMyDataAction= new InitMyDataAction(['destino 1', 'destino 2']);
        const newState: DestinosViajesState = reducerDestinosViajes(prevState, action);
        expect(newState.items.length).toEqual(2);
        expect(newState.items[0].nombre).toEqual('destino 1');
    });

    it('should reduce new item added', () => {
        const prevState: DestinosViajesState = initializeDestinosViajesState();
        const action: NuevoDestinoAction = new NuevoDestinoAction(new DestinoViaje('barcelona', 'url'));
        const newState: DestinosViajesState = reducerDestinosViajes(prevState, action);
        expect(newState.items.length).toEqual(1);
        expect(newState.items[0].nombre).toEqual('barcelona');
    });

    it('should reduce new item choose', () => {
        const prevState: DestinosViajesState = initializeDestinosViajesState();
        const d: DestinoViaje = new DestinoViaje('barcelona', 'url');
        const action: NuevoDestinoAction = new NuevoDestinoAction(d);
        let newState: DestinosViajesState = reducerDestinosViajes(prevState, action);
        const fav: ElegidoFavoritoAction = new ElegidoFavoritoAction(d);
        newState =  reducerDestinosViajes(newState, fav);;
        expect(newState.items.length).toEqual(1);
        expect(newState.items[0].selected).toEqual(true);
    });

    it('should reduce new item delete', () => {
        const prevState: DestinosViajesState = initializeDestinosViajesState();
        const d: DestinoViaje = new DestinoViaje('barcelona', 'url');
        const action: NuevoDestinoAction = new NuevoDestinoAction(d);
        let newState: DestinosViajesState = reducerDestinosViajes(prevState, action);
        const fav: EliminarDestinoAction = new EliminarDestinoAction(d);
        newState =  reducerDestinosViajes(newState, fav);
        expect(newState.items.length).toEqual(0);
    });

    it('should reduce new item vote down', () => {
        const prevState: DestinosViajesState = initializeDestinosViajesState();
        const d: DestinoViaje = new DestinoViaje('barcelona', 'url');
        const action: NuevoDestinoAction = new NuevoDestinoAction(d);
        let newState: DestinosViajesState = reducerDestinosViajes(prevState, action);
        const fav: VoteDownAction = new VoteDownAction(d);
        newState =  reducerDestinosViajes(newState, fav);
        expect(newState.items.length).toEqual(1);
        expect(newState.items[0].votes).toEqual(-1);
    });

    it('should reduce new item vote up', () => {
        const prevState: DestinosViajesState = initializeDestinosViajesState();
        const d: DestinoViaje = new DestinoViaje('barcelona', 'url');
        const action: NuevoDestinoAction = new NuevoDestinoAction(d);
        let newState: DestinosViajesState = reducerDestinosViajes(prevState, action);
        const fav: VoteUpAction = new VoteUpAction(d);
        newState =  reducerDestinosViajes(newState, fav);
        expect(newState.items.length).toEqual(1);
        expect(newState.items[0].votes).toEqual(1);
    });

    it('should reduce new item vote reset', () => {
        const prevState: DestinosViajesState = initializeDestinosViajesState();
        const d: DestinoViaje = new DestinoViaje('barcelona', 'url');
        const action: NuevoDestinoAction = new NuevoDestinoAction(d);
        let newState: DestinosViajesState = reducerDestinosViajes(prevState, action);
        const fav: VoteResetAction = new VoteResetAction(d);
        newState =  reducerDestinosViajes(newState, fav);
        expect(newState.items.length).toEqual(1);
        expect(newState.items[0].votes).toEqual(0);
    });

});