import { Directive, OnInit, OnDestroy } from '@angular/core';

@Directive({
    selector: '[appEspiame]'
})
export class EspiameDirective {
    static nextId = 0;
    log = (msg: string) => console.log(`Evento #${EspiameDirective.nextId++} ${msg}`);
    constructor() { }

    ngOnInit(){
        this.log('####**** OnInit');
    }

    ngOnDestroy(){
        this.log('####**** OnDestroy');
    }

}
