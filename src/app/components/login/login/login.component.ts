import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../../services/auth.service';

@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
    mensajeError: string = '';

    constructor(
        public authService: AuthService
    ) { }

    ngOnInit(): void {
    }

    logout() {
        this.authService.logout();
        return false;
    }

    login(userName: string, password: string): boolean {
        this.mensajeError = '';
        let self = this;
        if(!this.authService.login(userName, password)){
            this.mensajeError = 'Login incorrecto';
            setTimeout(() => {
                self.mensajeError = '';
            }, 2500);
        }
        return false;
    }

}
