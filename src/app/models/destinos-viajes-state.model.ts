import { Injectable } from '@angular/core';
import { Action } from "@ngrx/store";
import { Actions, Effect, ofType } from '@ngrx/effects';
import { DestinoViaje } from './destino-viaje.model';
import { Observable, of } from 'rxjs';
import { map } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';

export interface DestinosViajesState {
    items: DestinoViaje[];
    loading: boolean;
    favorito: DestinoViaje;
}

export function initializeDestinosViajesState(){
    return {
        items: [],
        loading: false,
        favorito: null
    };
};

// Actions
export enum DestinosViajesActionTypes {
    NUEVO_DESTINO = '[Destinos Viajes] Nuevo',
    ELEGIDO_FAVORITO = '[Destinos Viajes] Favorito',
    ELIMINAR_DESTINO = '[Destino Viajes] Eliminar',
    VOTE_UP = '[Destino Viajes] Vote Up',
    VOTE_DOWN = '[]Destino Viajes] Vote Down',
    VOTE_RESET = '[Destino Viajes] Vote Reset',
    INIT_MY_DATA = '[Destino Viajes] Init my data'
}

export class NuevoDestinoAction implements Action {
    type = DestinosViajesActionTypes.NUEVO_DESTINO;
    constructor(
        public destino: DestinoViaje
    ) { }
}

export class ElegidoFavoritoAction implements Action {
    type = DestinosViajesActionTypes.ELEGIDO_FAVORITO;
    constructor(
        public destino: DestinoViaje
    ) { }
}

export class EliminarDestinoAction{
    type = DestinosViajesActionTypes.ELIMINAR_DESTINO;
    constructor(public destino){ }
}

export class VoteDownAction{
    type = DestinosViajesActionTypes.VOTE_DOWN;
    constructor(public destino){ }
}

export class VoteUpAction{
    type = DestinosViajesActionTypes.VOTE_UP;
    constructor(public destino){ }
}

export class VoteResetAction{
    type = DestinosViajesActionTypes.VOTE_RESET;
    constructor(public destino){ }
}

export class InitMyDataAction{
    type = DestinosViajesActionTypes.INIT_MY_DATA;
    constructor(public destinos: string[]){ }
}

export type DestinosViajesActions = NuevoDestinoAction | ElegidoFavoritoAction | EliminarDestinoAction 
    | VoteDownAction | VoteUpAction | VoteResetAction | InitMyDataAction;

// Reducers
export function reducerDestinosViajes(
    state: DestinosViajesState,
    action: DestinosViajesActions
): DestinosViajesState{

    switch(action.type){
        case DestinosViajesActionTypes.INIT_MY_DATA: {
            const destinos: string[] = (action as InitMyDataAction).destinos;
            return {
                ...state,
                items: destinos.map((d) => new DestinoViaje(d, ''))
            }
        }
        case DestinosViajesActionTypes.NUEVO_DESTINO: {
            return {
                ...state,
                items: [...state.items, (action as NuevoDestinoAction).destino]
            }
        }
        case DestinosViajesActionTypes.ELEGIDO_FAVORITO : {
            state.items.forEach(x => x.setSelected(false));
            const fav: DestinoViaje = (action as ElegidoFavoritoAction).destino;
            fav.setSelected(true);
            return {
                ...state,
                favorito: fav
            }
        }
        case DestinosViajesActionTypes.ELIMINAR_DESTINO : {
            const destino: DestinoViaje = (action as EliminarDestinoAction).destino;
            const index: number = state.items.indexOf(destino);
            state.items.splice(index, 1);
            
            return {
                ...state
            }
        }
        case DestinosViajesActionTypes.VOTE_DOWN : {
            const destino: DestinoViaje = (action as VoteDownAction).destino;
            destino.setVotes('-');
            return {
                ...state
            }
        }
        case DestinosViajesActionTypes.VOTE_UP : {
            const destino: DestinoViaje = (action as VoteUpAction).destino;
            destino.setVotes('+');
            return {
                ...state
            }
        }
        case DestinosViajesActionTypes.VOTE_RESET : {
            const destino: DestinoViaje = (action as VoteResetAction).destino;
            destino.resetVotes();
            return {
                ...state
            }
        }
    }   
    
    return state;

}

// Effects
@Injectable()
export class DestinosViajesEffects{
    @Effect()
    nuevoAgregado$: Observable<Action> = this.actions$.pipe(
        ofType(DestinosViajesActionTypes.NUEVO_DESTINO),
        map((action: NuevoDestinoAction) => new ElegidoFavoritoAction(action.destino))
    )

    constructor(private actions$: Actions){ }

}