import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor() { }

  isLoggedIn(): boolean {
      console.log(this.getUser());
      return !!this.getUser();
  }

  logout() {
    localStorage.removeItem('username');
  }

  login(username: string, password: string): any {
    if(username === 'user' && password === 'password'){
        localStorage.setItem('username', username);
        //return true;
    }
    return false;
  }

  getUser(){
      return (localStorage.getItem('username') !== null);
  }

}
