import { BrowserModule } from '@angular/platform-browser';
import { NgModule, InjectionToken, Inject, APP_INITIALIZER, Injectable } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { environment } from '../environments/environment'; // Angular CLI environment
import { HttpClientModule, HttpClient, HttpHeaders, HttpRequest } from '@angular/common/http';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { NgxMapboxGLModule } from 'ngx-mapbox-gl';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { DestinoViajeComponent } from './components/destino-viaje/destino-viaje.component';
import { ListaDestinosComponent } from './components/lista-destinos/lista-destinos.component';
import { DestinoDetalleComponent } from './components/destino-detalle/destino-detalle.component';
import { FormDestinoViajeComponent } from './components/form-destino-viaje/form-destino-viaje.component';
import { DestinosApiClient } from './models/destinos-api-client.model';
import {
    DestinosViajesState,
    reducerDestinosViajes,
    initializeDestinosViajesState,
    DestinosViajesEffects,
    InitMyDataAction
} from './models/destinos-viajes-state.model';
import {
    StoreModule,
    ActionReducerMap,
    Store
} from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { LoginComponent } from './components/login/login/login.component';
import { ProtectedComponent } from './components/protected/protected/protected.component';
import { VuelosComponentComponent } from './components/vuelos/vuelos-component/vuelos-component.component';
import { VuelosMainComponentComponent } from './components/vuelos/vuelos-main-component/vuelos-main-component.component';
import { VuelosMasInfoComponentComponent } from './components/vuelos/vuelos-mas-info-component/vuelos-mas-info-component.component';
import { VuelosDetalleComponentComponent } from './components/vuelos/vuelos-detalle-component/vuelos-detalle-component.component';
import { ReservasModule } from './reservas/reservas.module';
import Dexie from 'dexie';
import { DestinoViaje } from './models/destino-viaje.model';
import { Observable, from } from 'rxjs';
import { flatMap } from 'rxjs/operators';
import { EspiameDirective } from './espiame.directive';
import { TrackerClickDirective } from './tracker-click.directive';

export class Translation {
    constructor(public id: number, public lang: string, public key: string, public value: string) {

    }
}

@Injectable({
    providedIn: 'root'
})
export class MyDatabase extends Dexie {
    destinos: Dexie.Table<DestinoViaje, number>;
    translations: Dexie.Table<Translation, number>;
    constructor() {
        super('MyDatabase');
        this.version(1).stores({
            destinos: '++id, nombre, imagenUrl'
        });
        this.version(2).stores({
            destinos: '++id, nombre, imagenUrl',
            translations: '++id, lang, key, value'
        });
    }
}

export const db = new MyDatabase();

class TranslationLoader implements TranslationLoader {
    constructor( private http: HttpClient){ }

    getTranslation(lang: string): Observable<any>{
        const promise = db.translations
            .where('lang')
            .equals(lang)
            .toArray()
            .then(results => {
                if(results.length === 0){
                    return this.http
                        .get<Translation[]>(APP_CONFIG_VALUE.apiEndpoint + '/api/translation?lang=' + lang)
                        .toPromise()
                        .then(apiResults => {
                            db.translations.bulkAdd(apiResults);
                            return apiResults;
                        })                
                }
                return results;
            }).then((traducciones) => {
                console.log('Traducciones cargadas');
                console.log(traducciones);
                return traducciones;
            }).then((traducciones) => {
                console.log(traducciones);
                return traducciones.map((t) => ({[t.key]: t.value}));
            });
        return from(promise).pipe(flatMap((elems) => from(elems)));
    }
    
}

function HttpLoaderFactory(http: HttpClient){
    return new TranslationLoader(http);
}


export interface AppConfig {
    apiEndpoint: string
}

export const APP_CONFIG_VALUE: AppConfig = {
    apiEndpoint: 'http://localhost:3000'
};

export const APP_CONFIG = new InjectionToken<AppConfig>('app.config');


// Redux init
export interface AppState {
    destinos: DestinosViajesState
}
const reducers: ActionReducerMap<AppState> = {
    destinos: reducerDestinosViajes
}
const reducersInitialState = {
    destinos: initializeDestinosViajesState()
};

export function init_app(appLoadService: AppLoadService): () => Promise<any> {
    return () => appLoadService.initializeDestinosViajesState();
};

@Injectable()
export class AppLoadService {
    constructor(
        private store: Store<AppState>,
        private http: HttpClient
    ) { }
    async initializeDestinosViajesState(): Promise<any> {
        const headers: HttpHeaders = new HttpHeaders({ 'X-API-TOKEN': 'token-seguridad' });
        const req = new HttpRequest('GET', APP_CONFIG_VALUE.apiEndpoint + '/my', { headers: headers });
        const response: any = await this.http.request(req).toPromise();
        this.store.dispatch(new InitMyDataAction(response.body));
    }
}

@NgModule({
    declarations: [
        AppComponent,
        DestinoViajeComponent,
        ListaDestinosComponent,
        DestinoDetalleComponent,
        FormDestinoViajeComponent,
        LoginComponent,
        ProtectedComponent,
        VuelosComponentComponent,
        VuelosMainComponentComponent,
        VuelosMasInfoComponentComponent,
        VuelosDetalleComponentComponent,
        EspiameDirective,
        TrackerClickDirective
    ],
    imports: [
        BrowserModule,
        AppRoutingModule,
        FormsModule,
        ReactiveFormsModule,
        HttpClientModule,
        NgxMapboxGLModule,
        BrowserAnimationsModule,
        StoreModule.forRoot(reducers, { initialState: reducersInitialState }),
        EffectsModule.forRoot([DestinosViajesEffects]),
        // Instrumentation must be imported after importing StoreModule (config is optional)
        StoreDevtoolsModule.instrument({
            maxAge: 25, // Retains last 25 states
            logOnly: environment.production, // Restrict extension to log-only mode
        }),
        ReservasModule,
        TranslateModule.forRoot({
            loader: {
                provide: TranslateLoader,
                useFactory: (HttpLoaderFactory),
                deps: [HttpClient] 
            }
        })
    ],
    providers: [
        DestinosApiClient,
        { provide: APP_CONFIG, useValue: APP_CONFIG_VALUE },
        AppLoadService, { provide: APP_INITIALIZER, useFactory: init_app, deps: [AppLoadService], multi: true },
        MyDatabase
    ],
    bootstrap: [AppComponent]
})
export class AppModule { }
