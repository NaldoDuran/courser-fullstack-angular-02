export class DestinoViaje {
    public selected: boolean = false;
    votes: number = 0; 
    servicios: string[] = ['pileta', 'desayuno'];
    id;

    constructor(
        public nombre: string,
        public url: string
    ) { }

    isSelected(){
        return this.selected;
    }

    setSelected(param: boolean = true){
        this.selected = param; 
    }

    setVotes(operation){
        if(operation === '+'){
            this.votes++;
        }else{
            this.votes--;
        }
        return false;
    }

    resetVotes(){
        this.votes = 0;
    }

}
