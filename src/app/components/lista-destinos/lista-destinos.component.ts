import { Component, OnInit } from '@angular/core';
import { DestinoViaje } from '../../models/destino-viaje.model';
import { DestinosApiClient } from '../../models/destinos-api-client.model';
import { Store } from '@ngrx/store';
import { AppState } from '../../app.module';

@Component({
    selector: 'app-lista-destinos',
    templateUrl: './lista-destinos.component.html',
    styleUrls: ['./lista-destinos.component.scss']
})
export class ListaDestinosComponent implements OnInit {

    updates: string[] = [];
    destinos: DestinoViaje[] = [];

    constructor(
        public destinosApiClient: DestinosApiClient,
        private store: Store<AppState>
    ) {
        this.store.select(state => state.destinos.items)
            .subscribe((d: any) => {
                this.destinos = d;
            });
        this.store.select(state => state.destinos.favorito)
            .subscribe(d => {
                if(d != null){
                    this.updates.push(`Se ha elegido a ${d.nombre}`);
                } 
            });
        /*this.destinosApiClient.subscribeOnChange((d: DestinoViaje) => {
            if(d != null){
                this.updates.push(`Se ha elegido a ${d.nombre}`);
            }
        });*/
    }

    ngOnInit(): void {
    }

    agregado(event: DestinoViaje): boolean {
        this.destinosApiClient.add(event);        
        return false;
    }

}
