import { Component, OnInit, InjectionToken, Inject, Injectable } from '@angular/core';
import { DestinosApiClient } from 'src/app/models/destinos-api-client.model';
import { ActivatedRoute } from '@angular/router';
import { DestinoViaje } from 'src/app/models/destino-viaje.model';
import { AppState } from 'src/app/app.module';
import { Store } from '@ngrx/store';


interface AppConfig {
    apiEndpoint: string
}

const APP_CONFIG_VALUE: AppConfig = {
    apiEndpoint: 'mi_api.com'
};

const APP_CONFIG = new InjectionToken<AppConfig>('app.config');

/*@Injectable()
export class DestinosApiClientDecorated extends DestinosApiClient {
    constructor(@Inject(APP_CONFIG) private config: AppConfig,  store: Store<AppState>){
        super(store);
    }
    getById(id: string): DestinoViaje{
        console.log('Llamando a la clase vieja 2');
        console.log(this.config.apiEndpoint);
        return super.getByd(id);
    } 
}*/

@Injectable()
export class DestinosApiClientViejo{
    getById(id: string): DestinoViaje{
        console.log('Llamando a la clase vieja');
        return null;
    }
}

@Component({
    selector: 'app-destino-detalle',
    templateUrl: './destino-detalle.component.html',
    styleUrls: ['./destino-detalle.component.scss'],
    providers: [
        DestinosApiClient
        /*{ provide: APP_CONFIG, useValue: APP_CONFIG_VALUE }, 
        { provide: DestinosApiClient, useClass: DestinosApiClientDecorated },
        { provide: DestinosApiClientViejo, useExisting: DestinosApiClient} */
    ]
})
export class DestinoDetalleComponent implements OnInit {
    destino: DestinoViaje;
    style = {
        sources: {
            world: {
                type: 'geojson',
                data: 'https://raw.githubusercontent.com/johan/world.geo.json/master/countries.geo.json'
            }
        }, 
        version: 8,
        layers: [{
            'id': 'countries',
            'type': 'fill',
            'source': 'world',
            'layout': {},
            'paint': {
                'fill-color': '#6F788A'
            }
        }]
    };
    

    constructor(
        private route: ActivatedRoute,
        private destinoApiClient: DestinosApiClient
    ) { }

    ngOnInit(): void {
        console.log(10);
        /*const id = this.route.snapshot.paramMap.get('id');
        this.destino = this.destinoApiClient.getById(id);*/
    }

}
