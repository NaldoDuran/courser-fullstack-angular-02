import { Component } from '@angular/core';
import { Observable } from 'rxjs';
import { TranslateService } from '@ngx-translate/core';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.scss']
})
export class AppComponent {
    title = 'angular';

    constructor(
        public translate: TranslateService
    ){
        translate.getTranslation('en').subscribe(x => console.log('x: ' + JSON.stringify(x)) );
        translate.setDefaultLang('es');
    }

    time = new Observable(Observer => {
        setInterval(() => Observer.next(new Date().toString()), 100);
    });

}
