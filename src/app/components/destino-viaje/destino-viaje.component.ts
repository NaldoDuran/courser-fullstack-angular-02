import { Component, OnInit, Input, HostBinding, EventEmitter, Output } from '@angular/core';
import { DestinoViaje } from '../../models/destino-viaje.model';
import { Store } from '@ngrx/store';
import { AppState } from '../../app.module';
import { DestinosApiClient } from '../../models/destinos-api-client.model';
import { trigger, state, style, transition, animate } from '@angular/animations';

@Component({
    selector: 'app-destino-viaje',
    templateUrl: './destino-viaje.component.html',
    styleUrls: ['./destino-viaje.component.scss'],
    animations: [
        trigger('esFavorito', [
            state('estadoFavorito', style({
                backgroundColor: 'PaleTurquoise'
            })),
            state('estadoNoFavorito', style({
                backgroundColor: 'whiteSmoke'
            })),
            transition('estadoNoFavorito => estadoFavorito', [
                animate('3s')
            ]),
            transition('estadoFavorito => estadoNoFavorito', [
                animate('1s')
            ])
        ])
    ]
})
export class DestinoViajeComponent implements OnInit {

    @Input() destino: DestinoViaje;
    @HostBinding('attr.class') cssClass = 'col-md-4 mb-4';
    @Output() clicked: EventEmitter<DestinoViaje> = new EventEmitter();
    @Output() eliminar: EventEmitter<DestinoViaje> = new EventEmitter();
    @Input() position: number;

    constructor(
        private store: Store<AppState>,
        private destinosApiClient: DestinosApiClient
    ) { }

    ngOnInit(): void {
    }

    seleccionarFavorito(): boolean {
        this.destinosApiClient.elegir(this.destino);
        return false;
    }

    eliminarAction(): boolean {
        this.destinosApiClient.eliminar(this.destino);
        return false;
    }

    voteDown(): boolean{
        this.destinosApiClient.voteDown(this.destino);
        return false;
    }

    voteUp(){
        this.destinosApiClient.voteUp(this.destino);
        return false;
    }

    voteReset(){
        this.destinosApiClient.voteReset(this.destino);
        return false;
    }

}
