import { DestinoViaje } from './destino-viaje.model';
import { BehaviorSubject, Subject } from 'rxjs';
import { Store } from '@ngrx/store';
import { AppState, APP_CONFIG, AppConfig, db } from '../app.module';
import { ElegidoFavoritoAction, NuevoDestinoAction, EliminarDestinoAction, VoteDownAction, VoteUpAction, VoteResetAction } from '../models/destinos-viajes-state.model';
import { Injectable, Inject, forwardRef } from '@angular/core';
import { HttpClient, HttpHeaders, HttpRequest, HttpResponse } from '@angular/common/http';

@Injectable()
export class DestinosApiClient {
    destinos: DestinoViaje[] = [];
    current: Subject<DestinoViaje> = new BehaviorSubject<DestinoViaje>(null);

    constructor(
        private store: Store<AppState>,
        @Inject(forwardRef(() => APP_CONFIG)) private config: AppConfig,
        private http: HttpClient
    ) {

    }

    add(d: DestinoViaje): void{
        const headers = new HttpHeaders({'X-API-TOKEN': 'token-seguridad'});
        const req = new HttpRequest('POST', this.config.apiEndpoint + '/my', {nuevo: d.nombre}, { headers: headers});
        this.http.request(req).subscribe((data: HttpResponse<{}>) => {
            if(data.status == 200){
                this.store.dispatch(new NuevoDestinoAction(d));
                const myDb = db;
                myDb.destinos.add(d);
                myDb.destinos.toArray().then(destinos => console.log(destinos));
            }
        })
        //this.destinos.push(d);
    }

    getAll(): DestinoViaje[]{
        return this.destinos;
    }

    getById(id: string): DestinoViaje{
        return this.destinos.filter((d: DestinoViaje) => d.id.toString() === id)[0];
    }

    elegir(d: DestinoViaje){
        /*this.destinos.forEach(x => x.setSelected(false));
        d.setSelected(true);
        this.current.next(d);*/     
        this.store.dispatch(new ElegidoFavoritoAction(d));
    }

    eliminar(d: DestinoViaje){
        this.store.dispatch(new EliminarDestinoAction(d));
    }

    subscribeOnChange(fn){
        this.current.subscribe(fn);
    }

    voteDown(d){
        this.store.dispatch(new VoteDownAction(d));
    }

    voteUp(d){
        this.store.dispatch(new VoteUpAction(d));
    }

    voteReset(d){
        this.store.dispatch(new VoteResetAction(d));
    }

}
